<?php

/**
 * Class Score
 *
 * It handles words and assigns weighted numbers to them.
 *
 * @package PhpScience\TextRank\Tool
 */
class Score
{
    /**
     * The maximum connections by a word in the current text.
     *
     * @var int
     */
    protected $maximumValue = 0;

    /**
     * The minimum connection by a word in the current text.
     *
     * @var int
     */
    protected $minimumValue = 0;

    /**
     * Calculate Scores.
     *
     * It calculates the scores from word's connections and the connections'
     * scores. It retrieves the scores in a form of a matrix where the key is
     * the word and value is the score. The score is between 0 and 1.
     *
     * @param Graph $graph The graph of the text.
     * @param Text  $text  Text object what stores all text data.
     *
     * @return array Key is the word and value is the float or int type score 
     *               between 1 and 0.
     */
    public function calculate(Graph $graph, Text &$text): array // tình điểm cho từ dựa vào liên kết ( điểm từ 0 -> 1)
    {
        $graphData = $graph->getGraph(); // lấy ra ma trận từ và connections
        $wordMatrix = $text->getWordMatrix(); // lấy ma trận từ trong 1 câu
        // print_r($wordMatrix);
        $wordConnections = $this->calculateConnectionNumbers($graphData); // tính số lượng connections của từ
        $scores = $this->calculateScores(
            $graphData,
            $wordMatrix,
            $wordConnections
        );

        return $this->normalizeAndSortScores($scores);
    }

    /**
     * Connection Numbers.
     *
     * It calculates the number of connections for each word and retrieves it
     * in array where key is the word and value is the number of connections.
     *
     * @param array $graphData Graph data from a Graph type object.
     *
     * @return array Key is the word and value is the number of the connected
     *               words.
     */
    protected function calculateConnectionNumbers(array &$graphData): array // tính số lương connections của từ
    {
        $wordConnections = [];

        foreach ($graphData as $wordKey => $sentences) {
            $connectionCount = 0;
            // print_r($wordKey);
            // print_r($sentences);
            // die();
            foreach ($sentences as $sentenceIdx => $wordInstances) { // duyệt ma trận connection của từng từ
                foreach ($wordInstances as $connections) {
                    $connectionCount += count($connections); //đếm số connection của từng từ
                    // print_r($connections);
                }
                // print_r($connectionCount);
                // die();
            }

            $wordConnections[$wordKey] = $connectionCount; // in ra từ kèm số connections
        }

        return $wordConnections;
    }

    /**
     * Calculate Scores.
     *
     * It calculates the score of the words and retrieves it in array where key
     * is the word and value is the score. The score depends on the number of
     * the connections and the closest word's connection numbers.
     *
     * @param array $graphData       Graph data from a Graph type object.
     * @param array $wordMatrix      Multidimensional array from integer keys
     *                               and string values.
     * @param array $wordConnections Key is the word and value is the number of
     *                               the connected words.
     *
     * @return array Scores where key is the word and value is the score.
     */
    protected function calculateScores(
        array &$graphData, // ma trận connection
        array &$wordMatrix, // ma trận từ của 1 câu
        array &$wordConnections // số connections của từ
    ): array {
        $scores = [];

        foreach ($graphData as $wordKey => $sentences) { // duyệt ma trận connection của từng từ
            $value = 0;
            // print_r($wordMatrix);
            // die();
            foreach ($sentences as $sentenceIdx => $wordInstances) {
                foreach ($wordInstances as $connections) {
                    foreach ($connections as $wordIdx) { // duyệt connections của từng từ
                        $word = $wordMatrix[$sentenceIdx][$wordIdx]; // lấy từ của câu trong ma trận từ ($centenceIdx => id của câu, $wordIdx => id của từ trong câu $centenceIdx) 
                        // print_r($word);
                        // die();
                        $value += $wordConnections[$word]; //tổng connections của từ trong bài
                    }
                }
            }

            $scores[$wordKey] = $value; // mảng gồm từ và tổng số kết nối của từ trong đoạn văn

            if ($value > $this->maximumValue) { // tìm số kết nối nhiều nhất
                $this->maximumValue = $value;
            }

            if ($value < $this->minimumValue || $this->minimumValue == 0) {
                $this->minimumValue = $value; // số kết nối it nhất
            }
        }

        return $scores;
    }

    /**
     * Normalize and Sort Scores.
     *
     * It recalculates the scores by normalize the score numbers to between 0
     * and 1.
     *
     * @param array $scores Keywords with scores. Score is the key.
     *
     * @return array Keywords with normalized and ordered scores.
     */
    protected function normalizeAndSortScores(array &$scores): array // tính lại điểm (điểm từ 0-1)
    {
        foreach ($scores as $key => $value) {
            $v = $this->normalize(
                $value,
                $this->minimumValue,
                $this->maximumValue
            );

            $scores[$key] = $v; //tính điểm cho từng từ
        }

        arsort($scores); // sắp xếp mảng theo điểm(sắp xếp theo value)

        return $scores;
    }

    /**
     * It normalizes a number.
     *
     * @param int $value Current weight.
     * @param int $min   Minimum weight.
     * @param int $max   Maximum weight.
     *
     * @return float|int Normalized weight aka score.
     */
    protected function normalize(int $value, int $min, int $max): float
    {
        $divisor = $max - $min; // tìm số chia (số connections max - connections min)

        if ($divisor == 0) { // nếu sô chia = 0 trả về 0
            return 0.0;
        }

        $normalized = ($value - $min) / $divisor; // số connections của từ - min / sô chia

        return $normalized;
    }
}