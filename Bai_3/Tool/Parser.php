<?php

/**
 * Class Parser
 *
 * This class purpose to parse a real text to sentences and array.
 *
 */
class Parser
{
    /**
     * The number of length of the smallest word. Words bellow it will be
     * ignored.
     *
     * @var int
     */
    protected $minimumWordLength = 0;

    /**
     * A single text, article, book for example.
     *
     * @var string
     */
    protected $rawText = '';

    /**
     * The array of the punctuations. The punctuation is the value. The key
     * refers to the key of its sentence.
     *
     * @var array
     */
    protected $marks = [];

    /**
     * Stop Words to ignore. These words will not be keywords.
     *
     * @var StopWordsAbstract
     */
    protected $stopWords;

    /**
     * It sets the minimum word length. Words bellow it will be ignored.
     *
     * @param int $wordLength
     */
    public function setMinimumWordLength(int $wordLength) // đặt độ dài tối thiểu. ngắn hơn bỏ qua
    {
        $this->minimumWordLength = $wordLength;
    }

    /**
     * It sets the raw text.
     *
     * @param string $rawText
     */
    public function setRawText(string $rawText) // lấy đoạn văn
    {
        $this->rawText = $rawText;
    }

    /**
     * Set Stop Words.
     *
     * It sets the stop words to remove them from the found keywords.
     *
     * @param StopWordsAbstract $words Stop Words to ignore. These words will
     *                                 not be keywords.
     */
    public function setStopWords(StopWordsAbstract $words) // lấy các từ khóa
    {
        $this->stopWords = $words;
    }

    /**
     * It retrieves the punctuations.
     *
     * @return array Array from punctuations where key is the index to link to
     *               the sentence and value is the punctuation.
     */
    public function getMarks(): array
    {
        return $this->marks;
    }

    /**
     * Parse.
     *
     * It parses the text from the property and retrieves in Text object
     * prepared to scoring and to searching.
     *
     * @return Text Parsed text prepared to scoring.
     */
    public function parse(): Text // tách văn bản để chuẩn bị tính điểm và tìm kiểm
    {
        $matrix = [];
        $sentences = $this->getSentences(); // danh sách các câu văn

        foreach ($sentences as $sentenceIdx => $sentence) { //duyệt các câu trong mảng
            $matrix[$sentenceIdx] = $this->getWords($sentence); // lấy các từ trong câu
        }

        $text = new Text();
        $text->setSentences($sentences); // các câu
        $text->setWordMatrix($matrix); // ma trận từ trong từng câu
        $text->setMarks($this->marks); //điểm

        return $text;
    }

    /**
     * Sentences.
     *
     * It retrieves the sentences in array without junk data.
     *
     * @return array Array from sentences.
     */
    protected function getSentences(): array // lấy các câu trong đoạn văn
    {
        $sentences = $sentences = preg_split('/(\n+)|(\\.\s)|(\\?\s)|(\\!\s)/', $this->rawText, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE); // lấy các câu trong đoạn văn

        return array_values(array_filter(array_map([$this, 'cleanSentence'], $sentences))); // lấy các câu văn đã clean (loại bỏ các câu không có ký tự)
    }

    /**
     * Possible Keywords.
     *
     * It retrieves an array of possible keywords without junk characters,
     * spaces and stop words.
     *
     * @param string $subText It should be a sentence.
     *
     * @return array The array of the possible keywords.
     */
    protected function getWords(string $subText): array // tách câu thành các từ
    {
        $words = preg_split(
            '/(?:(^\p{P}+)|(\p{P}*\s+\p{P}*)|(\p{P}+$))/',
            $subText,
            -1,
            PREG_SPLIT_NO_EMPTY
        );
        $words = array_values(
            array_filter(
                array_map(
                    [$this, 'cleanWord'],
                    $words
                )
            )
        );

        if ($this->stopWords) {
            return array_filter($words, function ($word) {
                return !ctype_punct($word)
                    && strlen($word) > $this->minimumWordLength
                    && !$this->stopWords->exist($word);
            });
        } else {
            return array_filter($words, function ($word) {
                return !ctype_punct($word)
                    && strlen($word) > $this->minimumWordLength;
            });
        }
    }

    /**
     * Clean Sentence.
     *
     * It clean the sentence. If it is a punctuation it will be stored in the
     * property $marks.
     *
     * @param string $sentence A sentence as a string.
     *
     * @return string It is empty string when it's punctuation. Otherwise it's
     *                the trimmed sentence itself.
     */
    protected function cleanSentence(string $sentence): string // làm sạch câu 
    {
        if (strlen(trim($sentence)) == 1) {
            $this->marks[] = trim($sentence);
            return '';
        } else {
            return trim($sentence); // trả về câu đã làm sạch
        }
    }

    /**
     * Clean Word.
     *
     * It removes the junk spaces from the word and retrieves it.
     *
     * @param string $word
     *
     * @return string Cleaned word.
     */
    protected function cleanWord(string $word): string
    {
        return mb_strtolower(trim($word));
    }
}