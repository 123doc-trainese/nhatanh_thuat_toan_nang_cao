<?php

/**
 * Class Graph
 *
 * This graph store the sentences and their words with the indexes. This graph
 * is the full map of the whole text.
 *
 */
class Graph
{
    /**
     * Key is the word, value is an array with the sentence IDs.
     *
     * @var array
     */
    protected $graph = [];

    /**
     * Create Graph.
     *
     * It creates a graph and save it into the graph property.
     *
     * @param Text $text Text object contains the parsed and prepared text
     *                   data.
     */
    public function createGraph(Text $text) //xây dựng ma trận từ và connections từng từ của câu
    {
        $wordMatrix = $text->getWordMatrix(); // lấy các từ trong 1 câu ( đã cắt từ khóa)
        foreach ($wordMatrix as $sentenceIdx => $words) { // duyệt mảng từ trong 1 câu
            $idxArray = array_keys($words); // lấy vị trí từ trong câu
            // echo "id câu: ";
            // print_r($sentenceIdx);
            // echo "\n";
            // echo "từ trong câu sau khi cắt từ khóa: \n";
            // print_r($words);
            // echo "\n";
            // print_r($idxArray);
            foreach ($idxArray as $idxKey => $idxValue) { // duyệt mảng vị trí từ
                $connections = []; // lấy từ nối của các từ
                // echo "id của từ: $words[$idxValue] là: ";
                // print_r($idxValue);
                // echo "\n";
                if (isset($idxArray[$idxKey - 1])) { // nếu có từ liền trước thì lấy connect là từ liên trước
                    $connections[] = $idxArray[$idxKey - 1];
                }

                if (isset($idxArray[$idxKey + 1])) { // mếu có từ liền sau thì lấy connections là từ liền sau
                    $connections[] = $idxArray[$idxKey + 1];
                }

                $this->graph[$words[$idxValue]][$sentenceIdx][$idxValue] = $connections; // trả về ma trận [từ]['id câu']['vị trí từ] = vị trí các từ nối

                // print_r($this->graph);
            }
            // die();
        }
    }

    /**
     * Graph.
     *
     * It retrieves the graph. Key is the word, value is an array with the
     * sentence IDs.
     *
     * <code>
     *       array(
     *           'apple' => array(    // word
     *               2 => array(      // ID of the sentence
     *                   52 => array( // ID of the word in the sentence
     *                       51, 53   // IDs of the closest words to the apple word
     *                   ),
     *                   10 => array( // IDs of the closest words to the apple word
     *                       9, 11    // IDs of the closest words to the apple word
     *                   ),
     *                   5 => array(6)
     *               ),
     *               6 => array(
     *                   9 => array(8, 10)
     *               ),
     *           ),
     *           'orange' => array(
     *               1  => array(
     *                   30 => array(29, 31)
     *               )
     *           )
     *       );
     * </code>
     *
     * @return array
     */
    public function getGraph(): array
    {
        return $this->graph;
    }
}