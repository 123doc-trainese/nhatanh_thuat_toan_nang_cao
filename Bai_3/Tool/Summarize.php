<?php

/**
 * Class Summarize
 *
 * This is for summarize the text from parsed data.
 *
 * @package PhpScience\TextRank\Tool
 */
class Summarize
{
    /**
     * To find all important sentences.
     *
     * @var int
     */
    const GET_ALL_IMPORTANT = 0;

    /**
     * To find the most important sentence and its following sentences.
     *
     * @var int
     */
    const GET_FIRST_IMPORTANT_AND_FOLLOWINGS = 1;

    /**
     * Array of sentence weight. Key is the index of the sentence and value is
     * the weight of the sentence.
     *
     * @var array
     */
    protected $sentenceWeight = [];

    /**
     * Summarize text.
     *
     * It retrieves the summarized text in array.
     *
     * @param array $scores        Keywords with scores. Score is the key. // từ cùng điểm của từ
     * @param Graph $graph         The graph of the text.  // ma trận từ của câu
     * @param Text  $text          Text object what stores all text data. // văn bản cần tóm tắt
     * @param int   $keyWordLimit  How many keyword should be used to find the
     *                             important sentences.  // số từ khóa để tìm câu tóm tắt
     * @param int   $sentenceLimit How many sentence should be retrieved. // số câu tóm tắt tối da
     * @param int   $type          The type of summarizing. Possible values are
     *                             the constants of this class.  // kiểu tóm tắt
     *
     * @return array An array from sentences.
     */
    public function getSummarize(
        array &$scores,
        Graph &$graph,
        Text &$text,
        int $keyWordLimit,
        int $sentenceLimit,
        int $type
    ): array {

        $graphData = $graph->getGraph();
        $sentences = $text->getSentences();
        $marks = $text->getMarks();
        $this->findAndWeightSentences($scores, $graphData, $keyWordLimit); // tính điểm cho từng câu

        if ($type == Summarize::GET_ALL_IMPORTANT) {
            return $this->getAllImportant($sentences, $marks, $sentenceLimit);
        } else if ($type == Summarize::GET_FIRST_IMPORTANT_AND_FOLLOWINGS) {
            return $this->getFirstImportantAndFollowings(
                $sentences,
                $marks,
                $sentenceLimit
            );
        }

        return [];
    }

    /**
     * Find and Weight Sentences.
     *
     * It finds the most important sentences and stores them into the property.
     *
     * @param array $scores       Keywords with scores. Score is the key.
     * @param array $graphData    Graph data from a Graph type object.
     * @param int   $keyWordLimit How many keyword should be used to find the
     *                            important sentences.
     */
    protected function findAndWeightSentences( // tìm và điền điểm cho từng câu
        array &$scores,
        array &$graphData,
        int $keyWordLimit
    ) {
        $i = 0;

        foreach ($scores as $word => $score) { // duyệt từ và điểm từng từ
            if ($i >= $keyWordLimit) { // kiểm tra số từ của 1 câu
                break;
            }

            $i++;
            $wordMap = $graphData[$word]; // lấy từ ma trận connection lấy từng từ
            // print_r($graphData); //
            // die();
            foreach ($wordMap as $key => $value) {
                $this->updateSentenceWeight($key); // tính điểm cho câu chứa từ
            }
        }

        arsort($this->sentenceWeight); // sắp xếp câu theo điểm
    }

    /**
     * Important Sentences.
     *
     * It retrieves the important sentences.
     *
     * @param array $sentences     Sentences, ordered by weights.
     * @param array $marks         Array of punctuations. Key is the reference
     *                             to the sentence, value is the punctuation.
     * @param int   $sentenceLimit How many sentence should be retrieved.
     *
     * @return array An array from sentences what are the most important
     *               sentences.
     */
    protected function getAllImportant( // lấy tất cả các câu quan trọng
        array &$sentences,
        array &$marks,
        int $sentenceLimit // giới hạn câu
    ): array {

        $summary = [];
        $i = 0;

        foreach ($this->sentenceWeight as $sentenceIdx => $weight) {
            if ($i >= $sentenceLimit) { // vượt quá sô câu thì thoát
                break;
            }

            $i++;
            $summary[$sentenceIdx] = $sentences[$sentenceIdx] // đưa ra câu kèm vị trí câu trong đoạn văn
                . $this->getMark($marks, $sentenceIdx); // thêm dấu câu ban đầu cho câu

            // print_r($this->getMark($marks, $sentenceIdx));
            // die();
        }

        ksort($summary); // sắp xếp lại câu

        return $summary;
    }

    /**
     * Most Important Sentence and Next.
     *
     * It retrieves the first most important sentence and its following
     * sentences.
     *
     * @param array $sentences     Sentences, ordered by weights.
     * @param array $marks         Array of punctuations. Key is the reference
     *                             to the sentence, value is the punctuation.
     * @param int   $sentenceLimit How many sentence should be retrieved.
     *
     * @return array An array from sentences what contains the most important
     *               sentence and its following sentences.
     */
    protected function getFirstImportantAndFollowings( // lấy câu điểm cao nhất  và các câu sau đó
        array &$sentences,
        array &$marks,
        int $sentenceLimit
    ): array {

        $summary = [];
        $startIdx = 0;

        foreach ($this->sentenceWeight as $sentenceIdx => $weight) {
            $summary[$sentenceIdx] = $sentences[$sentenceIdx] .
                $this->getMark($marks, $sentenceIdx);

            $startIdx = $sentenceIdx;
            break;
        }

        $i = 0;

        foreach ($sentences as $sentenceIdx => $sentence) {
            if ($sentenceIdx <= $startIdx) {
                continue;
            } else if ($i >= $sentenceLimit - 1) {
                break;
            }

            $i++;
            $summary[$sentenceIdx] = $sentences[$sentenceIdx] .
                $this->getMark($marks, $sentenceIdx);
        }

        return $summary;
    }

    /**
     * Update Sentence Weight.
     *
     * It updates the sentence weight what is stored in the property.
     *
     * @param int $sentenceIdx Index of the sentence.
     */
    protected function updateSentenceWeight(int $sentenceIdx)
    {
        if (isset($this->sentenceWeight[$sentenceIdx])) {
            $this->sentenceWeight[$sentenceIdx] = $this->sentenceWeight[$sentenceIdx] + 1;
        } else {
            $this->sentenceWeight[$sentenceIdx] = 1;
        }
    }

    /**
     * Punctuations.
     *
     * It retrieves the punctuation of the sentence.
     *
     * @param array $marks The punctuation. Key is the reference to the
     *                     sentence, value is the punctuation.
     * @param int   $idx   Key of the punctuation.
     *
     * @return string The punctuation of the sentence.
     */
    protected function getMark(array &$marks, int $idx) // 
    {
        return isset($marks[$idx]) ? $marks[$idx] : '';
    }
}