# Bài 3: Tóm tắt văn bản

- sử dụng thuật toán [TextRank](https://www.analyticsvidhya.com/blog/2018/11/introduction-text-summarization-textrank-python/) tóm tắt [văn bản](https://trello.com/1/cards/630880ef8b5e70010e975c87/attachments/63088a97c51e730197fbd814/download/text.txt) bằng cách nối 3 câu có điểm cao nhất

- sử dụng bộ [stopword.json](https://trello.com/1/cards/630880ef8b5e70010e975c87/attachments/63088a96dab43a015a863f37/download/stopword.json) 
## Ý tưởng thuật toán
    - tách văn bản thành từng câu
    - loại bỏ stopword khỏi từng câu
    - tạo ma trận [câu][từng trong câu]
    - tính connenction cho từng từ trong câu rồi lưu vào ma trận [từ][connenction]
    - tính điểm cho tưng từ dựa vào số connenction 
    - tính điểm cho câu dựa vào từ trong câu
    - tìm những câu có điểm cao nhất
    - sắp xếp câu theo thứ tự xuất hiện trong đoạn văn
## Link tham khảo
    - https://github.com/damminhtien/text-summarize
    - https://www.analyticsvidhya.com/blog/2018/11/introduction-text-summarization-textrank-python/