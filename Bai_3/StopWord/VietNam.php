<?php
include_once('StopWordsAbstract.php');
class VietName extends StopWordsAbstract
{
    protected $words = array(); // mảng từ khóa

    public function __construct($path)
    {
        $json = file_get_contents($path);
        $this->words = json_decode($json, true);
    }
}