<?php

include_once('TextRankFacade.php');
include_once('StopWord/VietNam.php');

$text = file_get_contents('text.txt'); //lấy nội dung đoạn văn

$stopwords = new VietName('stopword.json'); //lấy danh sách từ khóa

$tr = new TextRankFacade();

$tr->setStopwords($stopwords); //truyền lấy stopwords

$result = $tr->getHighlights($text); // lấy 12 câu có điểm cao nhất

// print_r(array_slice($result, 0, 3)); //

print_r($result); //
// $i = 0;
// foreach ($result as $a) {
//     if ($i < 3) echo $a . " ";
//     else break;
//     $i++;
// }