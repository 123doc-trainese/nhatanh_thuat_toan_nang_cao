# Bài 2: Extract keyword từ văn bản

- sử dụng thuật toản [Rake](https://www.analyticsvidhya.com/blog/2021/10/rapid-keyword-extraction-rake-algorithm-in-natural-language-processing/) extract keyword từ [văn bản](https://trello.com/1/cards/630880ef8b5e70010e975c87/attachments/63088a97c51e730197fbd814/download/text.txt)
- sử dụng bộ [stopword.json](https://trello.com/1/cards/630880ef8b5e70010e975c87/attachments/63088a96dab43a015a863f37/download/stopword.json) 

## Ý tưởng thuật toán 
    - chia bài viết thành từng cụm từ
    - loại bỏ từ stopword khỏi cụm từ
    - tách các cụm từ thành từng từ
    - xác định điểm cho từng từ
    - tính điểm cho từng cụm từ bằng cách cộng điểm từng từ trong cụm từ
## Link tham khảo
    - https://github.com/Richdark/RAKE-PHP
    - https://www.analyticsvidhya.com/blog/2021/10/rapid-keyword-extraction-rake-algorithm-in-natural-language-processing/