<?php
class Rake
{
    public $corpus;
    public $stopwords;
    public $word_degrees = [];
    public $word_frequencies = [];
    public $degree_scores = [];
    public $scores = [];

    public function __construct()
    {
        // Get content file
        $file = "text.txt";
        $fp = fopen($file, "r");
        $content = fread($fp, filesize($file));
        fclose($fp);

        // Get stopword
        $file = "stopwords.json";
        $fp = fopen($file, "r");
        $stopwords = fread($fp, filesize($file));
        $this->stopwords = json_decode($stopwords);
        fclose($fp);

        $content = preg_replace("/[0-9]+[.]?[0-9]*/", "|", $content);
        $this->corpus = preg_split("/(\.|, |,| - |\n|\(|\)|\"|[0-9]|%|\/)+/", $content);

        foreach ($this->corpus as $key => $value) {
            $this->corpus[$key] = trim($value);
            $this->corpus[$key] = strtolower($this->corpus[$key]);
            if ($this->corpus[$key] === "") {
                unset($this->corpus[$key]);
                continue;
            }

            $regex = '/\b' . implode('\b|\b', $this->stopwords) . '\b/iu';
            $this->corpus[$key] = preg_replace($regex, "|", $this->corpus[$key]);

            $this->corpus[$key] = explode('|', $this->corpus[$key]);
            foreach ($this->corpus[$key] as $key2 => $item) {
                if ($item == " " || $item == "") {
                    unset($this->corpus[$key][$key2]);
                    continue;
                }
                $this->corpus[$key][$key2] = trim($item);
                $words = explode(' ', trim($item));
                foreach ($words as $x) {
                    if (!array_key_exists($x, $this->word_degrees)) {
                        $this->word_degrees[$x] = 0;
                    }
                    foreach ($words as $x2) {
                        if (!array_key_exists($x2, $this->word_degrees)) {
                            $this->word_degrees[$x2] = 0;
                        }
                        $this->word_degrees[$x2]++;
                    }

                    if (!array_key_exists($x, $this->word_frequencies)) {
                        $this->word_frequencies[$x] = 0;
                    }
                    $this->word_frequencies[$x]++;
                }
            }
        }
    }

    public function calculate_degree_scores()
    {
        foreach ($this->word_frequencies as $key => $word) {
            $this->degree_scores[$key] = round($this->word_degrees[$key] / $this->word_frequencies[$key], 3);
        }
    }

    public function calculate_score()
    {
        foreach ($this->corpus as $key => $value) {
            foreach ($value as $a => $b) {
                $c = explode(' ', $b);
                $temp = 0;
                foreach ($c as $d => $e) {
                    $temp += $this->degree_scores[$e];
                }
                $this->scores[$b] = $temp;
            }
        }
        arsort($this->scores);
    }
}
echo "a";
$rake = new Rake();
$rake->calculate_degree_scores();
$rake->calculate_score();
print_r($rake->scores);