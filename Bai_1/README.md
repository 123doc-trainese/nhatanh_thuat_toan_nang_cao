# Bài 1: Tạo danh sách liên kết

## Đề bài 

Thiết kế việc triển khai danh sách liên kết của bạn. Bạn có thể chọn sử dụng một danh sách liên kết đơn hoặc kép.

Một nút trong danh sách được liên kết đơn lẻ phải có hai thuộc tính: `val` và `next`. `val` là giá trị của nút hiện tại và `next` là một con trỏ / tham chiếu đến nút tiếp theo.

Nếu bạn muốn sử dụng danh sách liên kết kép, bạn sẽ cần thêm một thuộc tính trước để chỉ ra nút trước đó trong danh sách được liên kết. 

Xây dựng class `MyLinkedList`:

- `get (int $index): int` : Nhận giá trị của nút chỉ mục trong danh sách liên kết. Nếu chỉ mục không hợp lệ, hãy trả về `-1`.
- `addAtHead(int val): void`: Thêm một nút có giá trị `val` trước phần tử đầu tiên của danh sách liên kết. Sau khi chèn, nút mới sẽ là nút đầu tiên của danh sách liên kết.
- `addAtTail(int val): void`: Nối một nút có giá trị `val` làm phần tử cuối cùng của danh sách được liên kết.
- `addAtIndex(int index, int val): void`: Thêm một nút có giá trị `val` trước nút `index` trong danh sách liên kết. Nếu `index` bằng độ dài của danh sách liên kết, nút sẽ được nối vào cuối danh sách liên kết. Nếu `index` lớn hơn độ dài, nút sẽ không được chèn.
- `deleteAtIndex(int index):void`: Xóa nút có vị trí `index` trong danh sách liên kết, nếu chỉ mục hợp lệ.
- `print(): void`: In ra danh sách liên kết
## Ý tưởng thuật toán
## Link tham khảo
- https://hocphp.net/tong-quan/thuc-hanh-trien-khai-lop-linkedlist-don-gian/