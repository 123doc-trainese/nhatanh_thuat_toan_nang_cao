<?php

class Node
{
    public $val; // giá trị node hiện tại
    public $next; // giá trị node tiếp theo

    public function __construct($val)
    {
        $this->val = $val; // giá trị node hiện tại = val
        $this->next = null; // node tiếp theo = null
    }

    public function readNode() //đọc node
    {
        return $this->val; //trả về giá trị node hiện tại
    }
}

class MyLinkedList
{
    private $head; //head

    public function addAtHead($val) // thêm node vào đầu danh sách
    {
        $link = new Node($val); // khởi tạo node mới
        if ($this->head === null) $this->head = $link;
        else {
            $link->next = $this->head; //
            $this->head = $this->link;
        }
    }

    public function addAtTail($val) // thêm node vào cuối danh sách
    {
        $link = new Node($val); // khởi tạo node mới
        if ($this->head === null) $this->head = $link;
        else {
            $last = $this->head;
            while ($last->next != null) $last = $last->next;
            $last->next = $link;
        }
    }

    public function addAtIndex($index, $val) // thêm node mới vào vị trí index
    {
        $link = new Node($val);
        if ($this->head == null) $this->head = $link;
        else {
            if ($index == 0) $this->addAtHead($val);
            else {
                $current = $this->head;
                $previous = $this->head;
                for ($i = 0; $i < $index; $i++) {
                    if ($current->next == null) return null;
                    else {
                        $previous = $current;
                        $current = $current->next;
                    }
                }
                $previous->next = $link;
                $link->next = $current;
            }
        }
    }

    public function deleteAtIndex(int $index) // xóa node tại index
    {
        $current = $this->head;    // gán current = node đầu tiên
        $previous = $this->head;   // gán previous = node đầu tiên
        if ($this->head === null) {
            echo 'linklist empty';
            return;
        }
        if ($index == 0) {
            if ($this->head->next !== null) $this->head = $this->head->next;
            else $this->head = NULL;
        } else {
            $index--;
            while ($index && $current->next != NULL) { // tìm vị trí index trên current    
                $index--;
                $previous = $current; // gán previous = current
                $current = $current->next; // gán current = current->next
            }
            if ($previous->next === null) { //
                echo 'index invalid';
            } else {
                $previous->next = $current->next; // gán current = current->next
            }
        }
    }

    public function get($index) // lấy node tại vị trí index
    {
        $current = $this->head; // gán current = node đầu tiên
        while ($index && $current != NULL) {
            $current = $current->next;
            $index--;
        }
        if ($current != NULL) return $current->val;
        else return NULL;
    }

    public function print() // in danh sánh node
    {
        $current = $this->head;
        while ($current != NULL) {
            echo $current->val;
            $current = $current->next;
        }
    }
}

$a = new MyLinkedList();
$a->addAtHead(11);
$a->addAtHead(22);
$a->addAtTail(33);
$a->addAtTail(44);
$a->addAtTail(43);
$a->addAtTail(21);
$a->addAtIndex(2, 2);
$a->deleteAtIndex(2);
$b = $a->get(45);
echo $b . "\n";
$a->print();